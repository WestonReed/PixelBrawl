#include<QApplication>
#include<QWidget>
#include<QtGui>
#include "common.h"
#include "fastTrig.h"
#include "graphics.h"

int main(int argc, char **argv) {

    QGuiApplication app(argc, argv);

    RasterWindow theWindow;
    theWindow.show();


    constructLookups();

    //region tests
    cout << getAngle(1, 0) << endl;
    cout << getAngle(0, 1) << endl;
    cout << getAngle(-1, 0) << endl;
    cout << getAngle(0, -1) << endl;
    cout << getAngle(3, 0) << endl;
    cout << getAngle(100, 200) << endl;
    cout << getAngle(-100, 200) << endl;
    cout << getAngle(-1, -1) << endl;
    //endregion

	return app.exec();
}