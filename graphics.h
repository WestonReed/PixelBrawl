//
// Created by kyle on 9/11/18.
//
#include <QtGui>

#ifndef PIXELBRAWL_GRAPHICS_H
#define PIXELBRAWL_GRAPHICS_H


class RasterWindow : public QWindow {
    Q_OBJECT
public:
    explicit RasterWindow(QWindow *parentWindow = nullptr);
    virtual void draw(QPainter *painter);

public slots:
    void drawLater();
    void drawNow();

protected:
    // Custom handling for different kinds of events
    bool event(QEvent *event) override;
    void resizeEvent(QResizeEvent *resizeEvent) override;
    void exposeEvent(QExposeEvent *event) override;

private:
    // The image buffer
    QBackingStore *m_backingStore;
};


#endif //PIXELBRAWL_GRAPHICS_H