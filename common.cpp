//
// Created by kyle on 9/18/18.
//

#include "common.h"

void timestampPrint(std::string message) {
	auto currTime = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> deltaT = currTime - progStart;
	printf("[%.6f] %s", deltaT.count(), message.c_str());
	std::cout << std::endl;
}