//
// Created by kyle on 9/10/18.
//

#include<chrono>
#include<iostream>
#include<string>

#ifndef PIXELBRAWL_COMMON_H
#define PIXELBRAWL_COMMON_H

const auto progStart = std::chrono::high_resolution_clock::now();

void timestampPrint(std::string message);

#endif //PIXELBRAWL_COMMON_H
