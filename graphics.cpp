//
// Created by kyle on 9/18/18.
//

#include "graphics.h"

// Implement the methods
RasterWindow::RasterWindow(QWindow *parentWindow)
		: QWindow(parentWindow)
		, m_backingStore(new QBackingStore(this)) {
	// Initial geometry for the window (x, y, w, h)
	setGeometry(100, 100, 300, 200);
}

void RasterWindow::exposeEvent(QExposeEvent *event) {
	if(isExposed()) {
		drawNow();
	}
}

void RasterWindow::resizeEvent(QResizeEvent *resizeEvent) {
	m_backingStore->resize(resizeEvent->size());
	if(isExposed()) {
		drawNow();
	}
}

void RasterWindow::drawNow() {
	// isExposed means visible
	if(!isExposed()) {
		return;
	}

	// drawing area
	QRect rect(0, 0, width(), height());
	m_backingStore->beginPaint(rect);

	QPaintDevice *paintDevice = m_backingStore->paintDevice();
	QPainter painter(paintDevice);

	// Start with a completely white canvas
	painter.fillRect(0, 0, width(), height(), Qt::white);
	draw(&painter);
	painter.end();

	m_backingStore->endPaint();
	m_backingStore->flush(rect);
}

void RasterWindow::draw(QPainter *painter) {
	// drawing happens here
	painter->drawText(QRectF(0, 0, width(), height()), Qt::AlignCenter, QStringLiteral("This is a test"));
}

void RasterWindow::drawLater() {
	requestUpdate();
}

bool RasterWindow::event(QEvent *event) {
	if(event->type() == QEvent::UpdateRequest) {
		drawNow();
		return true;
	}
	// fallback to QWindow's event handler
	return QWindow::event(event);
}