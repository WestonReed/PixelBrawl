//
// Created by kyle on 9/6/18.
//

#include <stdexcept>
#include <vector>

#ifndef PIXELBRAWL_PLAYER_H
#define PIXELBRAWL_PLAYER_H

#endif //PIXELBRAWL_PLAYER_H

class Player {
public:
    int playerId;
    int roomId;
    // lookAngle is a short because the math becomes much faster with shorts compared
    // to floating point. Range for the angle is 0 to USHRT_MAX, 0 being straight up
    // and USHRT_MAX being just CCW of it.
    unsigned short lookAngle;
    double *pos;

    explicit Player(int id) {
        this->playerId = id;
        this->roomId = 0;
        this->pos = new double[2] {0, 0};
        this->lookAngle = 0;
    }

    void setLookAngle(const std::vector<double> &mousePos) {
        if(mousePos.size() != 2) {
            auto err_msg = const_cast<char *>("");
            sprintf(err_msg,
                    "expected mousePos length 2; was %d",
                    static_cast<int>(mousePos.size()));
            throw std::invalid_argument(err_msg);
        }
        double dx = mousePos[0] - this->pos[0];
        double dy = mousePos[1] - this->pos[1];

    }
};