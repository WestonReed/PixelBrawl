//
// Created by kyle on 9/6/18.
//

#include<chrono>
#include<cmath>
#include<iostream>
#include "common.h"
using namespace std;

#ifndef PIXELBRAWL_FASTTRIG_H
#define PIXELBRAWL_FASTTRIG_H


static short sineLookup[65536];
static short cosineLookup[65536];
static unsigned short arcCosineLookup[65536];
static unsigned short pi = 32768;

extern int iSqrt(unsigned int x) {
    double sqrtX = sqrt(x);
    return static_cast<unsigned int>(sqrtX);
}

extern void constructLookups() {
    timestampPrint("Building lookups for sin(x)");
    for(int i = 0; i < 65536; i++) {
        double angle = static_cast<double>(i) / static_cast<double>(32768) * M_PI;
        double sine_angle = sin(angle);
        int convert_back = static_cast<int>(sine_angle * 32768);
        if(convert_back == 32768) { // fix bug where sin(pi/2) was -1 instead of 1
            convert_back = 32767;
        }
        sineLookup[i] = static_cast<short>(convert_back);
    }
    timestampPrint("Building lookups for cos(x)");
    for(int i = 0; i < 65536; i++) {
        short cosI = sineLookup[static_cast<unsigned short>(pi / 2 - i)];
        cosineLookup[i] = cosI;
        if(i > 32768) {
            continue;
        }
    }
    timestampPrint("Building lookups for arccos(x)");
    for(int i = -32768; i < 32768; i++) {
        // acos(x) \in [0, \pi].
        unsigned short acosI = static_cast<unsigned short>(acos(i / 32768.0) * 32768 / M_PI);
        arcCosineLookup[static_cast<unsigned short>(i)] = acosI;
    }
    arcCosineLookup[32768] = 0;
    timestampPrint("Done building lookups");
}

extern unsigned short fastAcos(short x) {
    return arcCosineLookup[x];
}

extern unsigned short getAngle(int dx, int dy) {
    int x = dx * 32768 * 181;
    x /= iSqrt(static_cast<unsigned int>(32768 * (dx * dx + dy * dy)));
    if(x == 32768) {
        x--;
    }
    unsigned short acosX = fastAcos(static_cast<short>(x));
    if(dy < 0) {
        acosX = static_cast<unsigned short>(2 * pi - acosX);
    }
    return acosX;
}

#endif //PIXELBRAWL_FASTTRIG_H